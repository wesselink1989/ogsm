import React, { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import classNames from 'classnames';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import DragableComponent from '../../components/DragableComponent';
import DropableComponent from '../../components/DropableComponent';

import './style.scss';

const columns = [
  {
    id: 'strategy',
    display: 'Strategie',
  },
  {
    id: 'dashboard',
    display: 'Dashboard',
  },
  {
    id: 'actions',
    display: 'Actieplan',
  },
];

const Ogsm = () => {
  const [rows, setRows] = useState([
    {
      id: uuidv4(),
    },
    {
      id: uuidv4(),
    },
    {
      id: uuidv4(),
    },
  ]);

  const [tickets, setTickets] = useState({
    ambitions: [],
    goals: [],
    dashboard: rows.map((x) => ({ [x.id]: [] })),
    strategy: rows.map((x) => ({ [x.id]: [] })),
    actions: rows.map((x) => ({ [x.id]: [] })),
  });

  const addItem = (index) => {
    const dummy = {
      id: uuidv4(),
      content:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam elementum dignissim convallis.',
    };
    if (columns.findIndex((x) => x.id === index) !== -1) {
      const arrayClone = tickets[index][rows[0].id];
      arrayClone.push(dummy);
      setTickets({
        [index]: arrayClone,
        ...tickets,
      });
      return;
    }
    const arrayClone = tickets[index];
    arrayClone.push(dummy);
    setTickets({
      [index]: arrayClone,
      ...tickets,
    });
  };

  const removeItem = (source, index) => {
    const sourceItems = tickets[source];
    sourceItems.splice(index, 1);
    setTickets({
      [source]: sourceItems,
      ...tickets,
    });
  };

  const onDragEnd = (result) => {
    const { source, destination, draggableId } = result;
    if (
      destination === null
      || destination.droppableId === source.droppableId
    ) {
      return;
    }
    if (destination.droppableId === 'empty') {
      setRows([...rows]);
      return;
    }

    if (columns.findIndex((x) => x.id === destination.droppableId) !== -1) {
      tickets[source.droppableId].forEach((x, i) => {
        const index = tickets[source.droppableId].findIndex(
          (x) => x.id === draggableId,
        );
        if (index !== -1) {
          let destinationArray = tickets[destination.droppableId][i];
          const key = Object.keys(destinationArray);
          destinationArray = destinationArray[key];
          destinationArray.push(tickets[source.droppableId][index]);
          const sourceItems = tickets[source.droppableId];
          sourceItems.splice(source.index, 1);
          setTickets({
            [source.droppableId]: sourceItems,
            [destination.droppableId]: destinationArray,
            ...tickets,
          });
        }
      });
      return;
    }

    const destinationItem = tickets[source.droppableId][source.index];
    const destinationArray = tickets[destination.droppableId];
    const sourceItems = tickets[source.droppableId];
    destinationArray.push(destinationItem);
    sourceItems.splice(source.index, 1);

    setTickets({
      [source.droppableId]: sourceItems,
      [destination.droppableId]: destinationArray,
      ...tickets,
    });
  };

  return (
    <div className={classNames('ogsm', 'container')}>
      <DragDropContext onDragEnd={onDragEnd}>
        <div className="row m-0">
          <DropableComponent title="Ambitie" add={() => addItem('ambitions')}>
            <Droppable droppableId="ambitions">
              {(provided) => (
                <div ref={provided.innerRef}>
                  {tickets.ambitions.map((item, index) => (
                    <Draggable
                      key={tickets.ambitions[index].id}
                      draggableId={tickets.ambitions[index].id}
                      index={index}
                    >
                      {(provided, snapshot) => (
                        <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                        >
                          <DragableComponent
                            moving={snapshot.isDragging}
                            content={item.content}
                            remove={() => removeItem('ambitions', index)}
                          />
                        </div>
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </DropableComponent>
        </div>

        <div className="row m-0 pt-3">
          <div className="col-lg-3 pl-0">
            <DropableComponent
              title="Doelstellingen"
              add={() => addItem('goals')}
            >
              <Droppable droppableId="goals">
                {(provided) => (
                  <div ref={provided.innerRef} className="droparea">
                    {tickets.goals.map((item, index) => (
                      <Draggable
                        key={tickets.goals[index].id}
                        draggableId={tickets.goals[index].id}
                        index={index}
                      >
                        {(provided, snapshot) => (
                          <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                          >
                            <DragableComponent
                              moving={snapshot.isDragging}
                              content={item.content}
                              remove={() => removeItem('goals', index)}
                            />
                          </div>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </DropableComponent>
          </div>

          <div className="col-lg-9 pl-3">
            <div className="row">
              {columns.map((category) => (
                <div className="col-lg-4 p-0">
                  <DropableComponent
                    title={category.display}
                    add={() => addItem(category.id)}
                  >
                    {rows.map((row, i) => (
                      <div className="row-borders">
                        <Droppable droppableId={category.id} key={row.id}>
                          {(provided) => (
                            <div
                              ref={provided.innerRef}
                              className="row-borders"
                            >
                              {tickets[category.id][i][row.id].map(
                                (item, index) => (
                                  <Draggable
                                    key={[category.id][i][row.id]}
                                    draggableId={
                                      tickets[category.id][i][row.id][index].id
                                    }
                                    index={index}
                                  >
                                    {(provided, snapshot) => (
                                      <div
                                        ref={provided.innerRef}
                                        {...provided.draggableProps}
                                        {...provided.dragHandleProps}
                                      >
                                        <DragableComponent
                                          moving={snapshot.isDragging}
                                          content={item.content}
                                          remove={() => removeItem(category.id, index)}
                                        />
                                      </div>
                                    )}
                                  </Draggable>
                                ),
                              )}
                              {provided.placeholder}
                            </div>
                          )}
                        </Droppable>
                      </div>
                    ))}
                  </DropableComponent>
                </div>
              ))}
              <div className="col-lg-12 p-0">
                <DropableComponent>
                  <Droppable droppableId="empty">
                    {(provided) => (
                      <div ref={provided.innerRef}>{provided.placeholder}</div>
                    )}
                  </Droppable>
                </DropableComponent>
              </div>
            </div>
          </div>
        </div>
      </DragDropContext>
    </div>
  );
};

export default Ogsm;
