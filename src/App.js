import React from 'react';
import { BrowserRouter, Switch } from 'react-router-dom';

import { getRoutes } from './routes/factory';

function App() {
  return (
    <BrowserRouter>
      <Switch>{getRoutes()}</Switch>
    </BrowserRouter>
  );
}

export default App;
