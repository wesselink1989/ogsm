import {
  OGSM,
} from '../constants/pages';

const paths = {
  [OGSM]: '/',
};

export default paths;
