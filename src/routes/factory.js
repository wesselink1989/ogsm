import React from 'react';
import { Route } from 'react-router-dom';

import Ogsm from '../pages/Ogsm';

import {
  OGSM,
} from '../constants/pages';

import paths from './paths';

const registry = {
  [OGSM]: Ogsm,
};

const getRoutes = () => (
  Object.keys(registry).map((pageId) => (
    <Route key={paths[pageId]} exact path={paths[pageId]} component={registry[pageId]} />
  ))
);

export { registry, getRoutes };
