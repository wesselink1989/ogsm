import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import "./style.scss";

const DragableComponent = ({ moving, content, remove }) => (
  <div className={classNames(moving && "moving", "dragable-component")}>
    <div className="remove" onClick={remove}/>
    {content}
  </div>
);

DragableComponent.propTypes = {
  content: PropTypes.string.isRequired,
  remove: PropTypes.isRequired,
  moving: PropTypes.bool,
};

DragableComponent.defaultProps = {
  moving: false,
};

export default DragableComponent;
