import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import "./style.scss";

const DropableComponent = ({ title, add, margin, children }) => (
  <div className={classNames(margin && "margin", "dropable-component")}>
    {title && (
      <>
        <h2>{title}</h2>
        <div className="add" onClick={add} />
      </>
    )}
    {children}
  </div>
);

DropableComponent.propTypes = {
  add: PropTypes.func.isRequired,
  rows: PropTypes.number.isRequired,
  children: PropTypes.node,
  title: PropTypes.string,
};

DropableComponent.defaultProps = {
  children: null,
  title: "",
};

export default DropableComponent;
